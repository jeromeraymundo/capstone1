$(document).ready(function() {

    $(window).scroll(function() {
        if ( $(this).scrollTop() ) {
            $("#backToTop").fadeIn();
        } else {
            $("#backToTop").fadeOut();
        }
    });

});